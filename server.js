//! Deps
const express = require("express");
const helmet = require("helmet");
const compression = require("compression");
const cors = require("cors");
const morgan = require("morgan");
const chalk = require("chalk");
const hbs = require("express-handlebars");
let { port, hostname } = {
  port: 50269
};
const con = require("./constants");

const app = express();

app.engine(
  "hbs",
  hbs({
    extname: "hbs",
    defaultView: "default"
  })
);

app.set("view engine", "hbs");
app.set("json spaces", 4);
app.use("/assets", express.static("./assets"));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true
  })
);
app.use(helmet());
app.use(compression());
app.use(cors());
// Logging
app.use(
  morgan((tokens, req, res) => {
    return [
      chalk.hex("#34ace0").bold(`[ ${tokens.method(req, res)} ]`),
      chalk.hex("#ffb142").bold(tokens.status(req, res)),
      chalk.hex("#ff5252").bold(req.hostname + tokens.url(req, res)),
      chalk.hex("#2ed573").bold(tokens["response-time"](req, res) + "ms"),
      chalk.hex("#f78fb3").bold("@ " + tokens.date(req, res))
    ].join(" ");
  })
);

function breakSocials(social) {
  // if (typeof social !== String) throw new Error('This social media is not a string :eyes:')
  return social.split("");
}

module.exports = async => {
  app.get("/", async (req, res) => {
    res.render("index", {
      layout: "main",
      name: con.name,
      host: req.hostname,
      social: [
        {
          name: "Twitter",
          link: "sor.dog/twitter",
          character: breakSocials("Twitter")
        },
        {
          name: "Spotify",
          link: "sor.dog/spotify",
          character: breakSocials("Spotify")
        },
        {
          name: "Soundcloud",
          link: "sor.dog/soundcloud",
          character: breakSocials("Soundcloud")
        },
        {
          name: "Patreon",
          link: "sor.dog/patreon",
          character: breakSocials("Patreon")
        },
        {
          name: "Bandcamp",
          link: "sor.dog/bandcamp",
          character: breakSocials("Bandcamp")
        },
        {
          name: "Youtube",
          link: "sor.dog/youtube",
          character: breakSocials("Youtube")
        },
        {
          name: "Discord",
          link: "sor.dog/discord",
          character: breakSocials("Discord")
        }
      ]
    });
  });

  app.get("/spotify", (req, res) => {
    res.redirect(con.socials.spotify);
  });
  app.get("/twitter", (req, res) => {
    res.redirect(con.socials.twitter);
  });
  app.get("/patreon", (req, res) => {
    res.redirect(con.socials.patreon);
  });
  app.get("/bandcamp", (req, res) => {
    res.redirect(con.socials.bandcamp);
  });
  app.get("/soundcloud", (req, res) => {
    res.redirect(con.socials.soundcloud);
  });
  app.get("/youtube", (req, res) => {
    res.redirect(con.socials.youtube);
  });
  app.get("/discord", (req, res) => {
    res.redirect(con.socials.discord);
  });

  app.listen(port /* , hostname */, () => {
    console.log(`[ Server ] Listening on ${port}`);
  });
};
