module.exports = {
  socials: {
    discord: "https://discord.gg/eXST5gv",
    bandcamp: "https://sorsor.bandcamp.com",
    patreon: "https://patreon.com/sorsor",
    youtube: "https://youtube.com/sorsor",
    twitter: "https://twitter.com/YaiSor",
    soundcloud: "https://soundcloud.com/yaisor",
    spotify: "https://open.spotify.com/artist/2gR5b7EpjCvePrAZZ3KQPj"
  }
};
